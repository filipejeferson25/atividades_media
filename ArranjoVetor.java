import java.util.Scanner;

public class Arranjo {

	public static void main(String[] args) {
	
		int qtde = 0;
		
		Scanner input = new Scanner(System.in);
		System.out.println("\nInforme a quantidade de números: ");
		qtde = input.nextInt();
		
		 int[] numeros = new int[qtde];
	        
	        // entrada de dados
	        for(int i = 0; i < numeros.length; i++){
	            System.out.println("Digite o " + (i + 1) + "° número");
	            numeros[i] = input.nextInt();
	        }
				 
			    for(int lista: numeros) {		
				System.out.printf("\nNúmero multiplicado por 10 = " + lista * 10);
			
			}
	        
	        // sada de dados
			    System.out.println("\n\n  ----   Arranjo invertido  ---- \n");
	        for(int i = numeros.length-1; i >= 0; i--){
	            System.out.println(numeros[i] * 10);
	       
		input.close();
	        }
	}
}

